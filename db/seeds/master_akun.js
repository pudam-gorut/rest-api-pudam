const tableName = require("../constant/tableName");

exports.seed = function (knex) {
  // Deletes ALL existing entries
  return knex(tableName.masterAkun)
    .del()
    .then(function () {
      // Inserts seed entries
      return knex(tableName.masterAkun).insert([
        { nama_akun: "Pendapatan", session_user: "" },
        { nama_akun: "Pengeluaran", session_user: "" },
        { nama_akun: "Aktiva Lancar", session_user: "" },
        { nama_akun: "Aktiva Tetap Berwujud", session_user: "" },
        { nama_akun: "Aktiva Tetap Tak Berwujud", session_user: "" },
        { nama_akun: "Investasi Jangka Panjang", session_user: "" },
      ]);
    });
};
