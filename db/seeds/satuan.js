const Knex = require("knex");
const tableName = require("../constant/tableName");

/**
 *
 * @param {Knex} knex
 */

exports.seed = async (knex) => {
  // Deletes ALL existing entries
  await knex(tableName.satuan)
    .del()
    .then(function () {
      // Inserts seed entries
      return knex(tableName.satuan).insert([
        {
          id: 1,
          nama_satuan: "Item",
          status: "N",
          user: "",
        },
        {
          id: 2,
          nama_satuan: "Unit",
          status: "N",
          user: "",
        },
        {
          id: 3,
          nama_satuan: "Pak",
          status: "N",
          user: "",
        },
        {
          id: 4,
          nama_satuan: "Dos",
          status: "N",
          user: "",
        },
        {
          id: 5,
          nama_satuan: "Paket",
          status: "N",
          user: "",
        },
        {
          id: 6,
          nama_satuan: "Pcs",
          status: "N",
          user: "",
        },
        {
          id: 7,
          nama_satuan: "Buah",
          status: "N",
          user: "",
        },
      ]);
    });
};
