const Knex = require("knex");
const tableName = require("../constant/tableName");

/**
 *
 * @param {Knex} knex
 */

exports.seed = async (knex) => {
  // Deletes ALL existing entries
  await knex(tableName.toko)
    .del()
    .then(function () {
      // Inserts seed entries
      return knex(tableName.toko).insert([
        {
          kode_toko: "TK001",
          nama_toko: "Toko 1",
          alamat: "Limboto",
          telp: "085398104825",
          status: "N",
          user: "",
        },
        {
          kode_toko: "TK002",
          nama_toko: "Toko 2",
          alamat: "Limboto",
          telp: "085398104825",
          status: "N",
          user: "",
        },

        {
          kode_toko: "TK003",
          nama_toko: "Toko 3",
          alamat: "Limboto",
          telp: "085398104825",
          status: "N",
          user: "",
        },
      ]);
    });
};
