const Knex = require("knex");
const tableName = require("../constant/tableName");

/**
 *
 * @param {Knex} knex
 */

exports.seed = async (knex) => {
  // Deletes ALL existing entries
  await knex(tableName.suplier)
    .del()
    .then(function () {
      // Inserts seed entries
      return knex(tableName.suplier).insert([
        {
          id: 1,
          kode_supplier: "SPL0001",
          nama_supplier: "Hermanto Lakoro",
          telp_supplier: "0853981048253",
          alamat_supplier: "Limboto",
          blokir: "N",
          user: "",
        },
        {
          id: 2,
          kode_supplier: "SPL0002",
          nama_supplier: "Harun Umar",
          telp_supplier: "0853981048253",
          alamat_supplier: "Limboto",
          blokir: "N",
          user: "",
        },
        {
          id: 3,
          kode_supplier: "SPL0003",
          nama_supplier: "Dimas",
          telp_supplier: "0853981048253",
          alamat_supplier: "Limboto",
          blokir: "N",
          user: "",
        },
      ]);
    });
};
