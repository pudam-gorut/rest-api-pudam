const Knex = require("knex");
const tableName = require("../constant/tableName");

/**
 *
 * @param {Knex} knex
 */

exports.seed = async (knex) => {
  // Deletes ALL existing entries
  await knex(tableName.kategori)
    .del()
    .then(function () {
      // Inserts seed entries
      return knex(tableName.kategori).insert([
        {
          id: 1,
          kategori: "HP",
          user: "",
        },
        {
          id: 2,
          kategori: "Accessoris",
          user: "",
        },
        {
          id: 3,
          kategori: "Headset",
          user: "",
        },
        {
          id: 4,
          kategori: "Powerbank",
          user: "",
        },
        {
          id: 5,
          kategori: "Speaker",
          user: "",
        },
        {
          id: 6,
          kategori: "Memori",
          user: "",
        },
        {
          id: 7,
          kategori: "Battery",
          user: "",
        },
        {
          id: 8,
          kategori: "Flasdisc",
          user: "",
        },
      ]);
    });
};
