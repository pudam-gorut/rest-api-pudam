module.exports = {
  pelanggan: "m_pelanggan",
  kabupaten: "t_kabupaten",
  kecamatan: "t_kecamatan",
  kelurahan: "t_kelurahan",
  tagihan: "t_tagihan",
  jenisPelanggan: "m_jenis_pelanggan",
  catatMeter: "t_catat_meter",
  targetPenagihan: "t_capaian_penagihan",
  pesan: "t_pesan",
  users: "users",
};
