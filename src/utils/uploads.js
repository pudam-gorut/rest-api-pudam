const moment = require("moment");
const multer = require("multer");

const storageFile = multer.diskStorage({
  destination: function (req, file, done) {
    done(null, "src/public/uploads/");
  },

  filename: function (req, file, done) {
    done(null, `${moment().format("yyyyMMDD_HHmmss")}_${file.originalname}`);
  },
});

const storageFoto = multer.diskStorage({
  destination: function (req, file, done) {
    console.log(__dirname);
    done(null, "src/public/uploads/");
  },

  filename: function (req, file, done) {
    done(null, `${moment().format("yyyyMMDD_HHmmss")}_${file.originalname}`);
  },
});

const uploadFile = multer({
  //   dest: "src/public/uploads/",
  storage: storageFile,
  limits: {
    fileSize: 1024 * 1024 * 10,
  },
  // fileFilter: filterFile,
});

const uploadFoto = multer({
  //   dest: "src/public/uploads/",
  storage: storageFoto,
  limits: {
    fileSize: 1024 * 1024 * 10,
  },
  // fileFilter: filterFile,
});

module.exports = {
  uploadFile,
  uploadFoto,
};
