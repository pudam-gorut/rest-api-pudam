const formatPhoneNumber = (number) => {
  // 1. menghilangkan karekter selain angka
  let formatted = number.replace(/\D/g, "");
  // 2. menghilangkan angka 0 kemudian merubah menjadi 62
  if (formatted.startsWith("0")) {
    formatted = "62" + formatted.substr(1);
  }

  if (!formatted.endsWith("@c.us")) {
    formatted += "@c.us";
  }

  return formatted;
};

module.exports = { formatPhoneNumber };
