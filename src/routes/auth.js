const express = require("express");
const route = express.Router();
// const { uploadFile } = require("../utils/uploads");
const { AuthController } = require("../controllers");

route.post("/login", AuthController.LoginPegawai);
route.post("/logout", AuthController.LoginPegawai);
route.get("/check/:session", AuthController.CheckUser);
route.get("/check-login", AuthController.CheckLogin);
route.get("/check-blokir/:session", AuthController.CheckBlokir);

module.exports = route;
