const express = require("express");
const route = express.Router();
const { MeterController } = require("../controllers");
const { uploadFoto } = require("../utils/uploads");

route.get("/", MeterController.GetPelanggan);
route.get("/history", MeterController.GetHistoryCatatMeter);
route.post(
  "/catat",
  uploadFoto.fields([
    { name: "foto_meter", maxCount: 1 },
    { name: "foto_segel", maxCount: 1 },
    { name: "foto_rumah", maxCount: 1 },
  ]),
  MeterController.CatatMeter
);

module.exports = route;
