const express = require("express");
const route = express.Router();
const { InfoController } = require("../controllers");

route.get("/meter", InfoController.GetInfoCatatMeter);
route.get("/tagihan", InfoController.GetInfoTagihan);

module.exports = route;
