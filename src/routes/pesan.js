const express = require("express");
const route = express.Router();
const { PesanController } = require("../controllers");

route.get("/", PesanController.GetPesanByUser);
route.get("/pegawai", PesanController.GetPegawai);
route.post("/kirim", PesanController.KirimPesan);
route.post("/kirim-pribadi", PesanController.KirimPesanPribadi);
route.put("/read/:id", PesanController.ReadPesan);

module.exports = route;
