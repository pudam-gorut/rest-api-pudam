const express = require("express");
const route = express.Router();

// route.use("/file", require("./file"));
route.use("/auth", require("./auth"));
// route.use(async (req, res, next) => {
//   try {
//     const token = req.headers["x-api-key"];
//     if (token !== "7qvt6t2738wrkjbykrfqxvk98") {
//       return res.status(400).json({
//         code: 400,
//         status: "Bad Request",
//         message: "akses anda ditolak",
//       });
//     }
//   } catch (error) {
//     return next(error);
//   }
// });

route.use("/pelanggan", require("./pelanggan"));
route.use("/tagihan", require("./tagihan"));
route.use("/meter", require("./meter"));
route.use("/info", require("./info"));
route.use("/pesan", require("./pesan"));

module.exports = route;
