const express = require("express");
const route = express.Router();
const { TagihanController } = require("../controllers");

route.get("/:id_user", TagihanController.GetTagihanById);
route.get("/pelanggan/:id_pelanggan", TagihanController.GetTagihanByPelanggan);
route.get("/total/:id_pelanggan", TagihanController.GetTotalTagihanByPelanggan);
route.get("/history/pelanggan", TagihanController.GetHistoryTagihan);
route.post("/bayar", TagihanController.BayarTagihan);
// route.get("/all/data", TagihanController.GetAll);

module.exports = route;
