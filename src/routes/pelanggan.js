const express = require("express");
const route = express.Router();
const { PelangganController } = require("../controllers");

route.get("/:id_kecamatan", PelangganController.GetPelangganById);

module.exports = route;
