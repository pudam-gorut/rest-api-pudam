const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const consola = require("consola");
const fs = require("fs");
const { Client, MessageMedia } = require("whatsapp-web.js");
const qrcode = require("qrcode");
const multer = require("multer");
const volleyball = require("volleyball");
const path = require("path");
const CronJob = require("cron").CronJob;
const WebResponse = require("./utils/WebResponse");
const { url } = require("inspector");
const app = express();
const server = require("http").createServer(app);
const io = require("socket.io")(server, {
  cors: {
    origin: "*",
  },
});

const { formatPhoneNumber } = require("./utils/formatter");
const { uploadFile } = require("./utils/uploads");

const SESSION_FILE_PATH = __dirname + "/../../whatsapp-session.json";
console.log(SESSION_FILE_PATH);
let sessionCfg;
if (fs.existsSync(SESSION_FILE_PATH)) {
  sessionCfg = require(SESSION_FILE_PATH);
}

const client = new Client({
  puppeteer: {
    headless: true,
    args: [
      "--no-sandbox",
      "--disable-setuid-sandbox",
      "--disable-dev-shm-usage",
      "--disable-accelerated-2d-canvas",
      "--no-first-run",
      "--no-zygote",
      "--single-process", // <- this one doesn't works in Windows
      "--disable-gpu",
    ],
  },
  session: sessionCfg,
});

client.on("authenticated", (session) => {
  console.log("AUTHENTICATED", session);
  sessionCfg = session;
  fs.writeFile(SESSION_FILE_PATH, JSON.stringify(session), function (err) {
    if (err) {
      console.log(err);
    }
  });
});

const checkRegisterNumber = async (number) => {
  const isRegister = await client.isRegisteredUser(number);
  return isRegister;
};

const PORT = process.env.PORT || 5000;
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());
// app.use(morgan("dev"));
app.use(volleyball);
// app.use(fileupload());
client.initialize();
io.on("connection", (socket) => {
  consola.success("Client Connection", socket.id);
  socket.emit("messages", "Connecting...");

  client.on("qr", (qr) => {
    qrcode.toDataURL(qr, (err, url) => {
      socket.emit("qr", url);
      socket.emit("messages", "QR Code tersedia, silahkan melakukan scaning!");
    });
  });

  client.on("ready", () => {
    socket.emit("messages", "Client is ready!");
  });
});

app.use((req, res, next) => {
  req.io = io;
  next();
});

app.get("/pesan", async (req, res, next) => {
  try {
    res.render("pages/index");
  } catch (error) {
    return next(error);
  }
});

app.use("/api/v1", require("./routes"));

app.get("/privacy", (req, res) => {
  return res.sendFile(__dirname + "/privacy.html");
});

app.get("/whatsapp", async (req, res, next) => {
  try {
    return res.render("pages/whatsapp");
  } catch (error) {
    return next(error);
  }
});

app.post("/send-messages", async (req, res, next) => {
  const { number, message } = req.body;
  try {
    if (number === "") {
      return WebResponse(res, 404, "Error", "Nomor HP tidak ada");
    }

    if (message === "") {
      return WebResponse(res, 404, "Error", "Pesan Masih kosong");
    }
    const isRegistered = await checkRegisterNumber(formatPhoneNumber(number));
    if (!isRegistered) {
      return WebResponse(
        res,
        404,
        "Error",
        "Nomor tidak terdaftar di WhatsApp"
      );
    }

    const kirim = await client.sendMessage(formatPhoneNumber(number), message);

    if (kirim) {
      return WebResponse(res, 200, "Success");
    }
  } catch (error) {
    return next(error);
  }
});

app.post("/send-files", uploadFile.single("file"), async (req, res, next) => {
  const { number, message } = req.body;
  const media = MessageMedia.fromFilePath(
    `${__dirname}/public/uploads/${req.file.filename}`
  );
  try {
    if (number === "") {
      return WebResponse(res, 404, "Error", "Nomor HP tidak ada");
    }

    if (message === "") {
      return WebResponse(res, 404, "Error", "Pesan Masih kosong");
    }
    const isRegistered = await checkRegisterNumber(formatPhoneNumber(number));
    if (!isRegistered) {
      return WebResponse(
        res,
        404,
        "Error",
        "Nomor tidak terdaftar di WhatsApp"
      );
    }

    const kirim = await client.sendMessage(formatPhoneNumber(number), media, {
      caption: message,
    });

    if (kirim) {
      return WebResponse(res, 200, "Success");
    }
  } catch (error) {
    return next(error);
  }
});

app.get("*", (req, res) => {
  return res.sendFile(__dirname + "/error.html");
});

function notFound(req, res, next) {
  res.status(404);
  const error = new Error("Not Found - " + req.originUrl);
  next(error);
}

function errorHandler(err, req, res, next) {
  res.status(res.statusCode || 500);
  return WebResponse(res, res.statusCode, err.message, err.stack);
}

app.use(notFound);
app.use(errorHandler);

server.listen(PORT, () => consola.success(`SERVER READY IN PORT ${PORT}`));
