const moment = require("moment");
const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

const GetTagihanById = async (req, res, next) => {
  const { id_user } = req.params;
  try {
    const dataTagihan = await db(tableName.tagihan)
      .sum({ total: "bayar" })
      .where({ id_user })
      .andWhere("tgl_bayar", moment().format("yyyy-MM-DD"));
    var count = 0;
    if (dataTagihan[0].total) {
      count = dataTagihan[0].total;
    } else {
      count = 0;
    }
    return WebResponse(res, 200, "Success", count.toString());
  } catch (error) {
    return next(error);
  }
};

const GetTagihanByPelanggan = async (req, res, next) => {
  const { id_pelanggan } = req.params;
  try {
    const dataPelanggan = await db(tableName.tagihan)
      .select(
        `${tableName.pelanggan}.id_pelanggan`,
        `${tableName.pelanggan}.no_sambungan`,
        `${tableName.pelanggan}.nama_pelanggan`,
        `${tableName.kelurahan}.kelurahan as alamat`,
        `${tableName.tagihan}.*`,
        `${tableName.jenisPelanggan}.jenis_pelanggan`,
        `${tableName.jenisPelanggan}.kd_jenis`
      )
      .where(`${tableName.tagihan}.id_pelanggan`, id_pelanggan)
      .join(
        tableName.pelanggan,
        `${tableName.tagihan}.id_pelanggan`,
        `${tableName.pelanggan}.id_pelanggan`
      )
      .join(
        tableName.jenisPelanggan,
        `${tableName.pelanggan}.id_jenis`,
        `${tableName.jenisPelanggan}.id_jenis`
      )
      .join(
        tableName.kelurahan,
        `${tableName.pelanggan}.id_kelurahan`,
        `${tableName.kelurahan}.id_kelurahan`
      )
      .orderBy("status", "desc")
      .orderBy("bulan_tagihan", "desc");

    const DATA = dataPelanggan.map((x) => {
      const nama_bulan = moment(x.bulan_tagihan).locale("id").format("MMMM");
      var bulan_lalu = 0;
      if (x.pemakaian_air_lalu === null) {
        let tarifDibawah = parseInt(x.tag_dibawah_10) + parseInt(x.by_admin);
        let a = parseInt(x.jlh_tagihan) - tarifDibawah;
        let b = a / parseInt(x.tarif_diatas_10);
        let c = b + 10;
        bulan_lalu = parseInt(x.pemakaian_air) - c;
      } else {
        bulan_lalu = parseInt(x.pemakaian_air_lalu);
      }

      return {
        id_pelanggan: x.id_pelanggan,
        no_sambungan: x.no_sambungan,
        nama_pelanggan: x.nama_pelanggan,
        alamat: x.alamat,
        id_pemakaian: x.id_pemakaian,
        no_rek: x.no_rek,
        id_kab: x.id_kab,
        id_kec: x.id_kec,
        id_kelurahan: x.id_kelurahan,
        id_jasa: x.id_jasa,
        id_jenis: x.id_jenis,
        tarif_dibawah_10: x.tarif_dibawah_10,
        tarif_diatas_10: x.tarif_diatas_10,
        bulan_tagihan: x.bulan_tagihan,
        pemakaian_air: x.pemakaian_air,
        pemakaian_air_lalu: Math.round(bulan_lalu),
        tag_dibawah_10: x.tag_dibawah_10,
        tag_diatas_10: x.tag_diatas_10,
        by_admin: x.by_admin,
        jlh_tagihan: x.jlh_tagihan,
        denda: x.denda,
        bayar: x.bayar,
        tgl_bayar: x.tgl_bayar,
        status: x.status,
        aktif: x.aktif,
        id_user: x.id_user,
        nama_bulan,
        kd_jenis: x.kd_jenis,
        jenis_pelanggan: x.jenis_pelanggan,
      };
    });
    return WebResponse(res, 200, "Success", DATA);
  } catch (error) {
    return next(error);
  }
};

const GetTotalTagihanByPelanggan = async (req, res, next) => {
  const { id_pelanggan } = req.params;
  try {
    const dataTagihan = await db(tableName.tagihan)
      .sum({ total: "jlh_tagihan" })
      .where({ id_pelanggan, status: "0" });
    // .andWhere({ status: 0 });
    var count = 0;
    if (dataTagihan[0].total) {
      count = dataTagihan[0].total;
    } else {
      count = 0;
    }
    return WebResponse(res, 200, "Success", count.toString());
  } catch (error) {
    return next(error);
  }
};

const BayarTagihan = async (req, res, next) => {
  const { bayar, tgl_bayar, id_pemakaian, id_user } = req.body;
  try {
    const update = await db(tableName.tagihan)
      .update({
        bayar,
        status: 1,
        id_user,
        tgl_bayar,
      })
      .where({ id_pemakaian });
    if (update) {
      return WebResponse(res, 201, "Success", "OK");
    } else {
      // return WebResponse(res, 201, "Error", "Err")
      return next(new Error("Pembayaran GAGAL"));
    }
  } catch (error) {
    return next(error);
  }
};

const GetHistoryTagihan = async (req, res, next) => {
  const { start, end, session } = req.query;
  try {
    const dataPelanggan = await db(tableName.tagihan)
      .select(
        `${tableName.pelanggan}.id_pelanggan`,
        `${tableName.pelanggan}.no_sambungan`,
        `${tableName.pelanggan}.nama_pelanggan`,
        `${tableName.pelanggan}.alamat`,
        `${tableName.tagihan}.*`,
        `${tableName.jenisPelanggan}.jenis_pelanggan`,
        `${tableName.jenisPelanggan}.kd_jenis`
      )
      .join(
        tableName.pelanggan,
        `${tableName.tagihan}.id_pelanggan`,
        `${tableName.pelanggan}.id_pelanggan`
      )
      .join(
        tableName.jenisPelanggan,
        `${tableName.pelanggan}.id_jenis`,
        `${tableName.jenisPelanggan}.id_jenis`
      )
      .where(`${tableName.tagihan}.id_user`, session)
      .whereBetween(`${tableName.tagihan}.tgl_bayar`, [
        moment(start).format("yyyy-MM-DD"),
        moment(end).format("yyyy-MM-DD"),
      ])
      .orderBy("status", "desc");

    const DATA = dataPelanggan.map((x) => {
      const nama_bulan = moment(x.bulan_tagihan).locale("id").format("MMMM");
      var bulan_lalu = 0;
      if (x.pemakaian_air_lalu === null) {
        let tarifDibawah = parseInt(x.tag_dibawah_10) + parseInt(x.by_admin);
        let a = parseInt(x.jlh_tagihan) - tarifDibawah;
        let b = a / parseInt(x.tarif_diatas_10);
        let c = b + 10;
        bulan_lalu = parseInt(x.pemakaian_air) - c;
      } else {
        bulan_lalu = parseInt(x.pemakaian_air_lalu);
      }

      return {
        id_pelanggan: x.id_pelanggan,
        no_sambungan: x.no_sambungan,
        nama_pelanggan: x.nama_pelanggan,
        alamat: x.alamat,
        id_pemakaian: x.id_pemakaian,
        no_rek: x.no_rek,
        id_kab: x.id_kab,
        id_kec: x.id_kec,
        id_kelurahan: x.id_kelurahan,
        id_jasa: x.id_jasa,
        id_jenis: x.id_jenis,
        tarif_dibawah_10: x.tarif_dibawah_10,
        tarif_diatas_10: x.tarif_diatas_10,
        bulan_tagihan: x.bulan_tagihan,
        pemakaian_air: x.pemakaian_air,
        pemakaian_air_lalu: Math.round(bulan_lalu),
        tag_dibawah_10: x.tag_dibawah_10,
        tag_diatas_10: x.tag_diatas_10,
        by_admin: x.by_admin,
        jlh_tagihan: x.jlh_tagihan,
        denda: x.denda,
        bayar: x.bayar,
        tgl_bayar: moment(x.tgl_bayar).locale("id").format(),
        status: x.status,
        aktif: x.aktif,
        id_user: x.id_user,
        nama_bulan,
        kd_jenis: x.kd_jenis,
        jenis_pelanggan: x.jenis_pelanggan,
      };
    });
    return WebResponse(res, 200, "Success", DATA);
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  GetTagihanById,
  GetTagihanByPelanggan,
  GetTotalTagihanByPelanggan,
  BayarTagihan,
  GetHistoryTagihan,
};
