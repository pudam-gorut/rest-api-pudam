const moment = require("moment");
const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

const GetInfoCatatMeter = async (req, res, next) => {
  const { session } = req.query;
  try {
    const bulanSekarang = moment().locale("id").format("MM");
    const tahunSekarang = moment().locale("id").format("yyyy");
    const dataTarget = await db(tableName.pelanggan)
      .count("petugas_meter", { as: "target" })
      .where("petugas_meter", session);

    const dataCapai = await db(tableName.catatMeter)
      .count("id_session", { as: "capaian" })
      .where("id_session", session)
      .where(db.raw("MONTH(tgl_catat) =?", parseInt(bulanSekarang)))
      .andWhere(db.raw("YEAR(tgl_catat) = ?", parseInt(tahunSekarang)));

    const data = {
      target: dataTarget[0].target,
      capaian: dataCapai[0].capaian,
    };

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

const GetInfoTagihan = async (req, res, next) => {
  const { session } = req.query;
  try {
    const bulanSekarang = moment().locale("id").format("MM");
    const tahunSekarang = moment().locale("id").format("yyyy");
    const target = await db(tableName.targetPenagihan)
      .select("target_capaian as target")
      .where("petugas", session)
      .andWhere("bulan", parseInt(bulanSekarang))
      .andWhere("tahun", parseInt(tahunSekarang));

    const capaian = await db(tableName.tagihan)
      .count("id_user", { as: "capaian" })
      .where("id_user", session)
      .andWhere(db.raw("MONTH(tgl_bayar)"), parseInt(bulanSekarang))
      .andWhere(db.raw("YEAR(tgl_bayar)"), parseInt(tahunSekarang));

    const DATA = {
      target: target[0].target,
      capaian: capaian[0].capaian,
    };

    return WebResponse(res, 200, "Success", DATA);
  } catch (error) {
    return next(error);
  }
};

module.exports = { GetInfoCatatMeter, GetInfoTagihan };
