const moment = require("moment");
const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

const GetPegawai = async (req, res, next) => {
  try {
    const data = await db(tableName.users)
      .select("nama_lengkap", "id_session")
      .where("level", "mobileloket");

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

const KirimPesan = async (req, res, next) => {
  const { jenis, pesan } = req.body;
  try {
    const tgl_pesan = moment().locale("id").format();
    await db(tableName.pesan).insert({ jenis, pesan, tgl_pesan });
    if (jenis == 1) {
      req.io.sockets.emit("pesan-semua", pesan);
    }
    if (jenis == 3) {
      req.io.sockets.emit("pesan-meter", pesan);
    }
    if (jenis == 4) {
      req.io.sockets.emit("pesan-tagihan", pesan);
    }

    return WebResponse(res, 200, "Success");
  } catch (error) {
    return next(error);
  }
};

const KirimPesanPribadi = async (req, res, next) => {
  const { id_session, jenis, pesan } = req.body;
  try {
    const tgl_pesan = moment().locale("id").format();
    await db(tableName.pesan).insert({ jenis, id_session, pesan, tgl_pesan });
    req.io.sockets.emit("pesan-pribadi", id_session);
    return WebResponse(res, 200, "Success");
  } catch (error) {
    return next(error);
  }
};

const GetPesanByUser = async (req, res, next) => {
  const { session } = req.query;
  try {
    const data = await db(tableName.pesan)
      //   .where("id_session", session)
      //   .whereIn("jenis", [1, 2, 3, 4])
      .orderBy("tgl_pesan", "desc");

    const dataArr = data.filter(
      (x) => x.id_session == session || x.id_session == null
    );

    const DATA = dataArr.map((x) => {
      return {
        id: x.id,
        jenis: x.jenis,
        id_session: x.id_session,
        pesan: x.pesan,
        status: x.status,
        tgl_pesan: moment(x.tgl_pesan).locale("id").calendar(),
      };
    });

    return WebResponse(res, 200, "Success", DATA);
  } catch (error) {
    return next(error);
  }
};

const ReadPesan = async (req, res, next) => {
  const { id } = req.params;
  try {
    await db(tableName.pesan).update({ status: 1 }).where({ id });
  } catch (error) {
    return next(error);
  }
};

module.exports = {
  GetPegawai,
  KirimPesan,
  KirimPesanPribadi,
  GetPesanByUser,
  ReadPesan,
};
