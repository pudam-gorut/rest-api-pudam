const moment = require("moment");
const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

const GetPelanggan = async (req, res, next) => {
  const { session } = req.query;
  try {
    const bulanSekarang = moment().locale("id").format("MM");
    const namaBulan = moment().locale("id").format("MMMM");
    const tahunSekarang = moment().locale("id").format("yyyy");
    const catatMeter = await db(tableName.catatMeter)
      .select(`${tableName.catatMeter}.id_pelanggan`)
      .where(db.raw("MONTH(tgl_catat) = ?", parseInt(bulanSekarang)))
      .andWhere(db.raw("YEAR(tgl_catat) = ?", parseInt(tahunSekarang)));

    var meterArr = [];
    var pelangganArr = [];

    catatMeter.map((x) => {
      meterArr.push(x.id_pelanggan);
    });

    // console.log(meterArr);

    // const dataTagihan = await db(tableName.tagihan)
    //   .where(db.raw("MONTH(bulan_tagihan) =?", parseInt(bulanSekarang)))
    //   .andWhere(db.raw("YEAR(bulan_tagihan) = ?", parseInt(tahunSekarang)))
    //   .whereNotIn("id_pelanggan", meterArr);
    // //   .toString();

    // dataTagihan.map((x) => {
    //   pelangganArr.push(x.id_pelanggan, ...meterArr);
    // });

    // console.log(pelangganArr);

    const tagihan = await db(tableName.tagihan)
      .select(
        `${tableName.tagihan}.id_pelanggan`,
        `${tableName.pelanggan}.no_sambungan`,
        `${tableName.pelanggan}.nama_pelanggan`,
        `${tableName.tagihan}.id_kec`,
        `${tableName.kecamatan}.kecamatan`,
        `${tableName.tagihan}.id_kelurahan`,
        `${tableName.kelurahan}.kelurahan`,
        `${tableName.jenisPelanggan}.jenis_pelanggan`,
        `${tableName.jenisPelanggan}.kd_jenis`
      )
      .join(
        tableName.kecamatan,
        `${tableName.tagihan}.id_kec`,
        `${tableName.kecamatan}.id_kec`
      )
      .join(
        tableName.kelurahan,
        `${tableName.tagihan}.id_kelurahan`,
        `${tableName.kelurahan}.id_kelurahan`
      )
      .join(
        tableName.pelanggan,
        `${tableName.tagihan}.id_pelanggan`,
        `${tableName.pelanggan}.id_pelanggan`
      )
      .join(
        tableName.jenisPelanggan,
        `${tableName.pelanggan}.id_jenis`,
        `${tableName.jenisPelanggan}.id_jenis`
      )
      .where(
        db.raw(
          `MONTH(${tableName.tagihan}.bulan_tagihan) =?`,
          parseInt(bulanSekarang)
        )
      )
      .andWhere(
        db.raw(
          `YEAR(${tableName.tagihan}.bulan_tagihan) = ?`,
          parseInt(tahunSekarang)
        )
      )
      .andWhere(`${tableName.pelanggan}.petugas_meter`, session)
      .andWhere(`${tableName.pelanggan}.status_aktif`, `1`)
      .whereNotIn(`${tableName.tagihan}.id_pelanggan`, meterArr)
      .groupBy(`${tableName.tagihan}.id_pelanggan`);

    // var adPelArr = [];
    // tagihan.map((x) => {
    //   adPelArr.push(x.id_pelanggan);
    // });

    const notRek = await db(tableName.pelanggan)
      .select(
        `${tableName.pelanggan}.id_pelanggan`,
        `${tableName.pelanggan}.no_sambungan`,
        `${tableName.pelanggan}.nama_pelanggan`,
        `${tableName.pelanggan}.status_aktif`,
        `${tableName.pelanggan}.id_kec`,
        `${tableName.kecamatan}.kecamatan`,
        `${tableName.pelanggan}.id_kelurahan`,
        `${tableName.kelurahan}.kelurahan`,
        `${tableName.jenisPelanggan}.jenis_pelanggan`,
        `${tableName.jenisPelanggan}.kd_jenis`
      )
      .join(
        tableName.kecamatan,
        `${tableName.pelanggan}.id_kec`,
        `${tableName.kecamatan}.id_kec`
      )
      .join(
        tableName.kelurahan,
        `${tableName.pelanggan}.id_kelurahan`,
        `${tableName.kelurahan}.id_kelurahan`
      )
      .join(
        tableName.jenisPelanggan,
        `${tableName.pelanggan}.id_jenis`,
        `${tableName.jenisPelanggan}.id_jenis`
      )
      .whereNotIn(`${tableName.pelanggan}.id_pelanggan`, meterArr)
      .andWhere(`${tableName.pelanggan}.petugas_meter`, session)
      .andWhere(`${tableName.pelanggan}.status_aktif`, `1`);

    // const notRek2 = await db(tableName.pelanggan)
    //   .select(
    //     `${tableName.pelanggan}.id_pelanggan`,
    //     `${tableName.pelanggan}.no_sambungan`,
    //     `${tableName.pelanggan}.nama_pelanggan`,
    //     `${tableName.pelanggan}.status_aktif`,
    //     `${tableName.jenisPelanggan}.jenis_pelanggan`,
    //     `${tableName.jenisPelanggan}.kd_jenis`
    //   )

    //   .join(
    //     tableName.jenisPelanggan,
    //     `${tableName.pelanggan}.id_jenis`,
    //     `${tableName.jenisPelanggan}.id_jenis`
    //   )
    //   .whereNotIn(`${tableName.pelanggan}.id_pelanggan`, [...adPelArr])
    //   .andWhere(`${tableName.pelanggan}.petugas_meter`, session)
    //   .andWhere(`${tableName.pelanggan}.status_aktif`, `1`);

    const dataA = [];
    await notRek.map((x) => {
      dataA.push({
        id_pelanggan: x.id_pelanggan,
        no_sambungan: x.no_sambungan,
        nama_pelanggan: x.nama_pelanggan,
        kecamatan: x.kecamatan,
        kelurahan: x.kelurahan,
        jenis_pelanggan: x.jenis_pelanggan,
        kd_jenis: x.kd_jenis,
        bulan: namaBulan,
      });
    });

    // await notRek2.map((x) => {
    //   dataA.push({
    //     id_pelanggan: x.id_pelanggan,
    //     no_sambungan: x.no_sambungan,
    //     nama_pelanggan: x.nama_pelanggan,
    //     jenis_pelanggan: x.jenis_pelanggan,
    //     kd_jenis: x.kd_jenis,
    //     bulan: namaBulan,
    //   });
    // });

    // const dataB = [];
    await tagihan.map((x) => {
      dataA.push({
        id_pelanggan: x.id_pelanggan,
        no_sambungan: x.no_sambungan,
        nama_pelanggan: x.nama_pelanggan,
        kecamatan: x.kecamatan,
        kelurahan: x.kelurahan,
        jenis_pelanggan: x.jenis_pelanggan,
        kd_jenis: x.kd_jenis,
        bulan: namaBulan,
      });
    });

    // const data = await db(tableName.pelanggan)
    //   .select(
    //     `${tableName.pelanggan}.id_pelanggan`,
    //     `${tableName.pelanggan}.no_sambungan`,
    //     `${tableName.pelanggan}.nama_pelanggan`,
    //     `${tableName.pelanggan}.status_aktif`,
    //     `${tableName.jenisPelanggan}.jenis_pelanggan`,
    //     `${tableName.jenisPelanggan}.kd_jenis`
    //   )
    //   .join(
    //     tableName.jenisPelanggan,
    //     `${tableName.pelanggan}.id_jenis`,
    //     `${tableName.jenisPelanggan}.id_jenis`
    //   )
    //   .join(
    //     tableName.tagihan,
    //     `${tableName.pelanggan}.id_pelanggan`,
    //     `${tableName.tagihan}.id_pelanggan`
    //   )
    //   .where(db.raw(`MONTH(${tableName.tagihan}.bulan_tagihan) =?`, parseInt(bulanSekarang)))
    //   .andWhere(db.raw(`YEAR(${tableName.tagihan}.bulan_tagihan) = ?`, parseInt(tahunSekarang)))
    //   .andWhere(`${tableName.pelanggan}.petugas_meter`, session)
    //   .andWhere(`${tableName.pelanggan}.status_aktif`, `1`);

    return WebResponse(res, 200, "Success", dataA);
  } catch (error) {
    return next(error);
  }
};

const CatatMeter = async (req, res, next) => {
  const {
    id_pelanggan,
    id_session,
    angka_meter,
    kondisi_meter,
    kondisi_segel,
    longitude,
    latitude,
    ada_meter,
  } = req.body;
  const { foto_meter, foto_rumah } = req.files;

  const bulanSekarang = moment().locale("id").format("MM");
  const tahunSekarang = moment().locale("id").format("yyyy");

  try {
    const checkData = await db(tableName.catatMeter)
      .where(db.raw("MONTH(tgl_catat)"), parseInt(bulanSekarang))
      .andWhere(db.raw("YEAR(tgl_catat)"), parseInt(tahunSekarang))
      .andWhere({ id_pelanggan });

    if (checkData.length > 0) {
      return WebResponse(
        res,
        200,
        "Error",
        "Meter Pelanggan ini sudah di catat"
      );
    }

    if (ada_meter === "Y") {
      await db(tableName.catatMeter).insert({
        id_pelanggan,
        id_session,
        tgl_catat: moment().format("yyyy-MM-DD"),
        foto_meter: foto_meter[0].filename,
        foto_rumah: "no_photo.png",
        angka_meter,
        kondisi_meter,
        kondisi_segel,
        longitude,
        latitude,
        ada_meter,
      });
      return WebResponse(res, 201, "Created", "Catat Meter Berhasil");
    } else {
      await db(tableName.catatMeter).insert({
        id_pelanggan,
        id_session,
        tgl_catat: moment().format("yyyy-MM-DD"),
        foto_meter: "no_photo.png",
        foto_rumah: foto_rumah[0].filename,
        angka_meter,
        kondisi_meter,
        kondisi_segel,
        longitude,
        latitude,
        ada_meter,
      });
      return WebResponse(res, 201, "Created", "Catat Meter Berhasil");
    }
  } catch (error) {
    return next(error);
  }
};

const GetHistoryCatatMeter = async (req, res, next) => {
  const { start, end, session } = req.query;
  try {
    const data = await db(tableName.catatMeter)
      .select(
        `${tableName.pelanggan}.nama_pelanggan`,
        `${tableName.pelanggan}.no_sambungan`,
        `${tableName.catatMeter}.*`
      )
      .join(
        tableName.pelanggan,
        `${tableName.catatMeter}.id_pelanggan`,
        `${tableName.pelanggan}.id_pelanggan`
      )
      .where("id_session", session)
      .whereBetween("tgl_catat", [
        moment(start).format("yyyy-MM-DD"),
        moment(end).format("yyyy-MM-DD"),
      ]);

    const DATA = data.map((x) => {
      return {
        id: x.id,
        nama_pelanggan: x.nama_pelanggan,
        no_sambungan: x.no_sambungan,
        id_pelanggan: x.id_pelanggan,
        id_session: x.id_session,
        tgl_catat: moment(x.tgl_catat).locale("id").format(),
        foto_meter: x.foto_meter,
        foto_segel: x.foto_segel,
        foto_rumah: x.foto_rumah,
        angka_meter: x.angka_meter,
        kondisi_meter: x.kondisi_meter,
        kondisi_segel: x.kondisi_segel,
        status_rekening: x.status_rekening,
        longitude: x.longitude,
        latitude: x.latitude,
        ada_meter: x.ada_meter,
      };
    });

    return WebResponse(res, 200, "Success", DATA);
  } catch (error) {
    return next(error);
  }
};

module.exports = { GetPelanggan, CatatMeter, GetHistoryCatatMeter };
