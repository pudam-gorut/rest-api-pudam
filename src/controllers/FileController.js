// const importExcell = require("convert-excel-to-json");
// const del = require("del");
// const db = require("../../db");
// const tableName = require("../../db/constant/tableName");
// const WebResponse = require("../utils/WebResponse");

// const UploadFile = (req, res, next) => {
//   const filename = req.file.filename;
//   const dataImport = importExcell({
//     sourceFile: __dirname + "/../public/uploads/" + filename,
//     header: { rows: 1 },
//     columnToKey: {
//       A: "NO",
//       B: "KODE_BARANG",
//       C: "NAMA_BARANG",
//       D: "SATUAN_BESAR",
//       E: "ISI",
//       F: "SATUAN_KECIL",
//       G: "KPS",
//       H: "KANDUNGAN",
//       I: "HRG_DASAR",
//       J: "HRG_BELI",
//       K: "RALAN",
//       L: "KELAS_1",
//       M: "KELAS_2",
//       N: "KELAS_3",
//       O: "UTAMA",
//       P: "KELAS_VIP",
//       Q: "KELAS_VVIP",
//       R: "BELI_LUAR",
//       S: "JUAL_BEBAS",
//       T: "KARYAWAN",
//       U: "STOK_MINIMUM",
//     },
//     sheets: ["Sheet1"],
//   });
//   const DATA = dataImport.Sheet1.map((x) => {
//     return {
//       kode_brng: x.KODE_BARANG,
//       nama_brng: x.NAMA_BARANG,
//       kode_satbesar: "1",
//       kode_sat: "4",
//       letak_barang: "-",
//       dasar: x.HRG_DASAR === undefined ? 0 : x.HRG_DASAR,
//       h_beli: x.HRG_BELI,
//       ralan: x.RALAN,
//       kelas1: x.KELAS_1,
//       kelas2: x.KELAS_2,
//       kelas3: x.KELAS_3,
//       utama: x.UTAMA,
//       vip: x.KELAS_VIP,
//       vvip: x.KELAS_VVIP,
//       beliluar: x.BELI_LUAR,
//       jualbebas: x.JUAL_BEBAS,
//       karyawan: x.KARYAWAN,
//       stokminimal: x.STOK_MINIMUM,
//       kdjns: "-",
//       isi: x.ISI === undefined ? 0 : x.ISI,
//       kapasitas: 0,
//       expire: "2020-04-25",
//       status: "1",
//       kode_industri: "-",
//       kode_kategori: "-",
//       kode_golongan: "-",
//     };
//   });

//   db(tableName.databarang)
//     .insert(DATA)
//     .then((d) => {
//       return WebResponse(res, 200, "Success", d);
//     })
//     .catch((err) => {
//       return next(err);
//     });

//   // return db.transaction((trx) => {
//   //   var queries = [];
//   //   DATA.map((x) => {
//   //     const query = db(tableName.stok_toko)
//   //       .where({ id: x.id })
//   //       .update({ harga_jual: x.harga_jual })
//   //       .increment({ stok_akhir: x.stok_akhir })
//   //       .transacting(trx);
//   //     queries.push(query);
//   //   });

//   //   return Promise.all(queries)
//   //     .then(async () => {
//   //       await trx.commit;
//   //       await del([__dirname + "/../public/uploads/stok/*.xlsx"], {
//   //         dryRun: true,
//   //       });
//   //       return WebResponse(res, 201, "Success");
//   //     })
//   //     .catch((e) => {
//   //       trx.rollback;
//   //       return next(e);
//   //     });
//   // });
// };

// const GetDataFile = async (req, res, next) => {
//   try {
//     const data = await db(tableName.databarang).select("*");
//     return WebResponse(res, 200, "Success", data);
//   } catch (error) {
//     return next(error);
//   }
// };

// module.exports = { UploadFile, GetDataFile };

// // knex.transaction(function(trx) {
// //   knex('users')
// //       .transactiong(trx)
// //       .select('id')
// //       .select('firstName')
// //       .then(async function(res) {
// //           for (let i = 0; i < res.length; i++) {
// //               const userData = res[i]; //here we have id and firstName
// //               const encryptedFirstName = encrypt(userData.firstName);
// //               trx('users')
// //                   .where('id', '=', userData.id)
// //                   .update({
// //                       firstName: encryptedFirstName
// //                   })
// //           }
// //       })
// //       .then(function() {
// //          trx.commit();
// //          process.exit(0);
// //       })
// //       .catch(function(err) {
// //          trx.rollback();
// //          process.exit(1);
// //       })
// // });
