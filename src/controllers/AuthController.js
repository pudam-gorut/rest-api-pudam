const md5 = require("md5");
const jwt = require("jsonwebtoken");
const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");
// const calcMD5 = require("../utils/calcMD5");

const LoginPegawai = async (req, res, next) => {
  const { username, password } = req.body;
  try {
    const checkUsername = await db(tableName.users).where({ username });
    const pass = md5(password);
    if (checkUsername.length <= 0) {
      return WebResponse(res, 200, "Error", "username tidak terdaftar");
    }

    if (checkUsername[0].password !== pass) {
      return WebResponse(
        res,
        200,
        "Error",
        "Username atau password anda salah"
      );
    }

    // if (checkUsername[0].level !== "mobileloket") {
    //   return WebResponse(
    //     res,
    //     200,
    //     "Error",
    //     "Anda tidak punya akses ke sini, silahkan hubungi admin"
    //   );
    // }

    if (checkUsername[0].blokir === "Y") {
      return WebResponse(
        res,
        200,
        "Error",
        "Maaf anda sudah di blokir, silahkan hubungi admin"
      );
    }

    const dataUsers = await db(tableName.users)
      .select(
        `${tableName.users}.username`,
        `${tableName.users}.nama_lengkap`,
        `${tableName.users}.no_telp`,
        `${tableName.users}.jenis_kelamin`,
        `${tableName.users}.id_session`,
        `${tableName.kabupaten}.id_kab`,
        `${tableName.kabupaten}.kabupaten`,
        `${tableName.kecamatan}.id_kec`,
        `${tableName.kecamatan}.kecamatan`,
        `${tableName.kelurahan}.id_kelurahan`,
        `${tableName.kelurahan}.kelurahan`,
        `${tableName.users}.alamat`,
        `${tableName.users}.level`,
        `${tableName.users}.mobile_akses`,
        `${tableName.users}.id_kategori`
      )
      .join(
        tableName.kabupaten,
        `${tableName.users}.id_kab`,
        `${tableName.kabupaten}.id_kab`
      )

      .join(
        tableName.kecamatan,
        `${tableName.users}.id_kec`,
        `${tableName.kecamatan}.id_kec`
      )
      .join(
        tableName.kelurahan,
        `${tableName.users}.id_desa`,
        `${tableName.kelurahan}.id_kelurahan`
      )
      .where({ username: checkUsername[0].username });

    await db(tableName.users)
      .update({ id_kategori: 1 })
      .where({ id_session: dataUsers[0].id_session });

    const token = await jwt.sign({ ...dataUsers[0] }, "7qvt6t2738");
    console.log(token);

    return WebResponse(res, 200, "Success", token);
  } catch (error) {
    console.log(error);
    return next(error);
  }
};

const CheckUser = async (req, res, next) => {
  const { session } = req.params;
  console.log(session);
  try {
    const user = await db(tableName.users)
      .select("id_kategori")
      .where("id_session", session);
    return WebResponse(res, 200, "Success", user[0].id_kategori);
  } catch (error) {
    return next(error);
  }
};

const CheckLogin = async (req, res, next) => {
  const { session } = req.query;
  try {
    const check = await db(tableName.users)
      .select("blokir")
      .where("id_session", session);

    return WebResponse(res, 200, "Success", check[0].blokir);
  } catch (error) {
    return next(error);
  }
};

const CheckBlokir = async (req, res, next) => {
  const { session } = req.params;
  try {
    req.io.sockets.emit("check-blokir", session);
    return WebResponse(res, 200, "Success");
  } catch (error) {
    return next(error);
  }
};

module.exports = { LoginPegawai, CheckUser, CheckLogin, CheckBlokir };
