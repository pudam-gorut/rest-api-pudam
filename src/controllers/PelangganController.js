const db = require("../../db");
const tableName = require("../../db/constant/tableName");
const WebResponse = require("../utils/WebResponse");

const GetPelangganById = async (req, res, next) => {
  const { id_kecamatan } = req.params;
  try {
    const data = await db(tableName.pelanggan)
      .select(
        `${tableName.pelanggan}.id_pelanggan`,
        `${tableName.pelanggan}.no_sambungan`,
        `${tableName.pelanggan}.nama_pelanggan`,
        `${tableName.pelanggan}.aktif`,
        `${tableName.pelanggan}.alamat`,
        `${tableName.pelanggan}.tgl_terdaftar`,
        `${tableName.pelanggan}.status_aktif`,
        `${tableName.pelanggan}.lat`,
        `${tableName.pelanggan}.long`,
        `${tableName.pelanggan}.id_kab`,
        `${tableName.kabupaten}.kabupaten`,
        `${tableName.pelanggan}.id_kec`,
        `${tableName.kecamatan}.kecamatan`,
        `${tableName.pelanggan}.id_kelurahan`,
        `${tableName.kelurahan}.kelurahan`,
        `${tableName.jenisPelanggan}.jenis_pelanggan`,
        `${tableName.jenisPelanggan}.kd_jenis`
      )
      .join(
        tableName.kabupaten,
        `${tableName.pelanggan}.id_kab`,
        `${tableName.kabupaten}.id_kab`
      )
      .join(
        tableName.kecamatan,
        `${tableName.pelanggan}.id_kec`,
        `${tableName.kecamatan}.id_kec`
      )
      .join(
        tableName.kelurahan,
        `${tableName.pelanggan}.id_kelurahan`,
        `${tableName.kelurahan}.id_kelurahan`
      )
      .join(
        tableName.jenisPelanggan,
        `${tableName.pelanggan}.id_jenis`,
        `${tableName.jenisPelanggan}.id_jenis`
      )
      .where(`${tableName.pelanggan}.id_kec`, id_kecamatan);

    return WebResponse(res, 200, "Success", data);
  } catch (error) {
    return next(error);
  }
};

module.exports = { GetPelangganById };
