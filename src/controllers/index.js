// const FileController = require("./FileController");
const AuthController = require("./AuthController");
const PelangganController = require("./PelangganController");
const TagihanController = require("./TagihanController");
const MeterController = require("./MeterController");
const InfoController = require("./InfoController");
const PesanController = require("./PesanController");

module.exports = {
  AuthController,
  PelangganController,
  TagihanController,
  MeterController,
  InfoController,
  PesanController,
};
