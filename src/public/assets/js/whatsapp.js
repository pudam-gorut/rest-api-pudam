const SOCKET_URL = "http://pudam.ip-dynamic.com:5000";
$(document).ready(function () {
  var socket = io.connect(SOCKET_URL, { path: "/socket.io" });
  socket.on("messages", function (msg) {
    $(".logs").append($("<li>").text(msg));
  });

  socket.on("qr", function (src) {
    $("#qrcode").attr("src", src);
  });
});
