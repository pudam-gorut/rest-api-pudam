var BASE_URL = "http://pudam.ip-dynamic.com:5000/api/v1";
// var BASE_URL = "http://localhost:5000/api/v1";
$(document).ready(function () {
  $(".js-example-basic-single").select2();

  var user = $(".pilih-petugas");
  if ($("#jenis-pesan").val() == 1) {
    user.hide();
  }

  $("#jenis-pesan").on("change", function (e) {
    if (e.target.value == 2) {
      user.show();
      getPegawai();
    } else {
      user.hide();
      $("#user option").remove();
    }
  });

  $("#user").on("change", function (e) {
    console.log(e.target.value);
  });

  $("#btn-kirim").on("click", function () {
    if ($("#jenis-pesan").val() == 2) {
      if ($("#user").val() == 0) {
        swal({
          title: "Error!",
          text: "Petugas belum dipilih",
          icon: "error",
        });
      } else {
        if ($("#pesan").val() == "") {
          swal({
            title: "Error!",
            text: "Pesan masih kosong",
            icon: "error",
          });
        } else {
          kirimPesanPribadi(
            $("#user").val(),
            $("#jenis-pesan").val(),
            $("#pesan").val()
          );
        }
      }
    } else {
      if ($("#pesan").val() == "") {
        swal({
          title: "Error!",
          text: "Pesan masih kosong",
          icon: "error",
        });
      } else {
        kirimPesan($("#jenis-pesan").val(), $("#pesan").val());
      }
    }
  });
});

function kirimPesan(jenis, pesan) {
  $.ajax({
    method: "POST",
    url: `${BASE_URL}/pesan/kirim`,
    data: { jenis, pesan },
  }).done(function (res) {
    swal({
      title: "Berhasil!",
      text: "Pessan berhasil terkirim",
      icon: "success",
    });

    kosong();
  });
}

function kirimPesanPribadi(id_session, jenis, pesan) {
  $.ajax({
    method: "POST",
    url: `${BASE_URL}/pesan/kirim-pribadi`,
    data: { id_session, jenis, pesan },
  }).done(function (res) {
    swal({
      title: "Berhasil!",
      text: "Pessan berhasil terkirim",
      icon: "success",
    });

    kosong();
  });
}

function getPegawai() {
  $.ajax({
    method: "GET",
    url: `${BASE_URL}/pesan/pegawai`,
  }).done(function (res) {
    $("#user").append(`<option value="0">Pilih Pegawai</option>`);
    res.data.map(function (item) {
      $("#user").append(`
        <option value="${item.id_session}">
            ${item.nama_lengkap}
        </option>
      `);
    });
  });
}

function kosong() {
  $("#user").val(0);
  $(".pilih-petugas").hide();
  $("#jenis-pesan").val(1);
  $("#pesan").val("");
}

// var data = new FormData()
// data.append("number","085398104825") // nomor hp
// data.append("message","test pesan") // pesan
// data.append("file", file) // data file

// $.ajax({
//   url: "http://pudam.ip-dynamic.com:5000/send-files",
//   method: "POST",
//   data: data,
//   success: function() {
//     console.log("Kirim FIle berhasil")
//   }
// })
